package com.dcatalan.coleccion;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by davidcatalanlisa on 3/11/16.
 */
public class Musica implements Serializable{

    public enum GeneroCD {
        ROCK,POP,RAP,TECNO
    }
    public enum TipoCD {
        VINILO,CD
    }
    String codMusica;
    String artista;
    Date año;
    TipoCD tipoCD;
    GeneroCD generoCD;

    public Musica() {

    }

    public String getCodMusica() {
        return codMusica;
    }

    public void setCodMusica(String codMusica) {
        this.codMusica = codMusica;
    }

    public String getArtista() {
        return artista;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public Date getAño() {
        return año;
    }

    public void setAño(Date año) {
        this.año = año;
    }

    public TipoCD getTipoCD() {
        return tipoCD;
    }

    public void setTipoCD(TipoCD tipoCD) {
        this.tipoCD = tipoCD;
    }

    public GeneroCD getGeneroCD() {
        return generoCD;
    }

    public void setGeneroCD(GeneroCD generoCD) {
        this.generoCD = generoCD;
    }

    @Override
    public String toString() {
        return codMusica;
    }
}
