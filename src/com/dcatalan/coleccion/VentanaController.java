package com.dcatalan.coleccion;

import com.dcatalan.coleccion.*;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by davidcatalanlisa on 3/11/16.
 */
public class VentanaController implements ActionListener{

    private Ventana view;
    private VentanaModel model;

    public VentanaController(Ventana view, VentanaModel model) {

        this.view=view;
        this.model=model;

        addActionListener();
    }

    private void addActionListener() {
        view.btLibro.addActionListener(this);
        view.btMusica.addActionListener(this);
        view.btPelicula.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String comando = e.getActionCommand();

        switch(comando){
            case "Libro":
                VentanaLibro vLibro = new VentanaLibro();
                LibroController libroController = new LibroController(vLibro,model);
                break;
            case "Pelicula":
                VentanaPeliculas vPeliculas = new VentanaPeliculas();
                PeliculaController peliculaController = new PeliculaController(vPeliculas,model);
                break;
            case "Musica":
                VentanaMusica vMusica= new VentanaMusica();
                MusicaController musicaController = new MusicaController(vMusica,model);
                break;
        }
    }
}
