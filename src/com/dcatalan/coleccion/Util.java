package com.dcatalan.coleccion;

import javax.swing.*;
import java.text.DecimalFormat;
import java.text.ParseException;

/**
 * Created by davidcatalanlisa on 9/11/16.
 */
public class Util {

    /**
     * METODO PARA PASAR LA EDAD A UN FORMATO CONCRETO
     *Y METODO  MENSAJE DE ERROR
     */
    private static final String PATRON_EDAD="###000";
    public static void mensajeError(String mensaje, String titulo) {
        JOptionPane.showMessageDialog(null, mensaje, titulo, JOptionPane.ERROR_MESSAGE);
    }

    public static int parseEdad(String edad) throws ParseException {
        DecimalFormat df=new DecimalFormat(PATRON_EDAD);
        return df.parse(edad).intValue();
    }

    public static java.sql.Date getDateSql(java.util.Date fecha) {
        return new java.sql.Date(fecha.getTime());
    }

}
