package com.dcatalan.coleccion;

import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Created by davidcatalanlisa on 3/11/16.
 */
public class VentanaPeliculas {
    JTextField tfCodPelicula;
    JTextField tfDirector;
    JComboBox cbGeneroPeliculas;
    JComboBox cbTipoPe;
    JButton btAlta;
    JButton btModificar;
    JButton btEliminar;
    JList listPeliculas;
    JButton btTerminar;
    JPanel panelPeliculas;
    JDateChooser dtPeliculas;
    JTextField tfBusquedaPelicula;
    JTable tablePeliculas;
    JScrollPane sbPeliculas;
    JFrame frame;
    DefaultTableModel dtmPeliculas;


    public VentanaPeliculas() {
        frame = new JFrame("Ventana Peliculas");
        frame.setContentPane(panelPeliculas);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        dtmPeliculas = new DefaultTableModel(){
            @Override
            public boolean isCellEditable (int row, int column){
                return false;
            }
        };

        dtmPeliculas.addColumn("Codigo Peliucula");
        dtmPeliculas.addColumn("Director");
        dtmPeliculas.addColumn("Año");
        dtmPeliculas.addColumn("Genero Pelicula");
        dtmPeliculas.addColumn("Tipo Pelicula");

        tablePeliculas.setModel(dtmPeliculas);
    }
}
