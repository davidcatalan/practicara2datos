package com.dcatalan.coleccion;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringJoiner;

import static com.dcatalan.coleccion.VentanaModel.libros;

/**
 * Created by davidcatalanlisa on 3/11/16.
 */
public class LibroController implements ActionListener,ListSelectionListener,KeyListener {
    
    private VentanaLibro viewLibro;
    private VentanaModel model;

    public LibroController(VentanaLibro vLibro, VentanaModel model) {

        this.viewLibro=vLibro;
        this.model=model;

        addActionListener();
        addSelectionListeners();
        addKeyListener();

        rellenarComboBox();

        inicializarTabla();

        refrescarTabla();
    }

    private void inicializarTabla() {
        viewLibro.tableLibros.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ListSelectionModel lsm = viewLibro.tableLibros.getSelectionModel();
        lsm.addListSelectionListener(this);
    }

    private void addKeyListener() {
        viewLibro.tfBusquedaLibro.addKeyListener(this);
    }

    private void rellenarComboBox() {
        viewLibro.cbGeneroLibros.addItem("<Seleccciona el genero>");
        for(Libro.GeneroLi genero : Libro.GeneroLi.values()){
            viewLibro.cbGeneroLibros.addItem(genero.name());
        }
    }

    /**
     * AÑADIR LISTENERS
     */
    private void addSelectionListeners() {
//        viewLibro.tableLibros.addListSelectionListener(this);
    }

    private void addActionListener() {
        viewLibro.btAlta.addActionListener(this);
        viewLibro.btEliminar.addActionListener(this);
        viewLibro.btModificar.addActionListener(this);
        viewLibro.btTerminar.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String comando = e.getActionCommand();
        Libro nuevoLibro= null;
        Date fecha=null;
        int fila = viewLibro.tableLibros.getSelectedRow();

        switch(comando){
            case "Alta":
                modoEdicion(true);

                if(viewLibro.tfCodLibro.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Codigo Libro OBLIGATORIO",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                if(viewLibro.cbGeneroLibros.getSelectedIndex()==0){
                    JOptionPane.showMessageDialog(null, "Selecciona Genero",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if( viewLibro.dtLibro.getDate() == null) {
                    viewLibro.dtLibro.setDate( new Date());
                }
                if(viewLibro.tfAutor.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Selecciona Autor",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if(viewLibro.tfEdad.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Selecciona Edad",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                fecha = viewLibro.dtLibro.getDate();
                nuevoLibro = new Libro();

                nuevoLibro.setCodLibro(viewLibro.tfCodLibro.getText());
                nuevoLibro.setAutor(viewLibro.tfAutor.getText());
                nuevoLibro.setAño(fecha);
                nuevoLibro.setGeneroLi(Libro.GeneroLi.valueOf((String) viewLibro.cbGeneroLibros.getSelectedItem()));
                nuevoLibro.setEdad(Integer.parseInt(viewLibro.tfEdad.getText()));
                model.registrarLibro(nuevoLibro);

                limpiarGUI();
                refrescarTabla();
                modoEdicion(true);
                break;

            case "Modificar":
                modoEdicion(true);
                if(viewLibro.tfCodLibro.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Codigo Libro OBLIGATORIO",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                if(viewLibro.cbGeneroLibros.getSelectedIndex()==0){
                    JOptionPane.showMessageDialog(null, "Selecciona Genero",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if( viewLibro.dtLibro.getDate() == null) {
                    viewLibro.dtLibro.setDate( new Date());
                }
                if(viewLibro.tfAutor.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Selecciona Autor",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if(viewLibro.tfEdad.getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Selecciona Edad",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                //COGER EL CODIGO DE LIBRO PARA
                //String codigo = viewLibro.tableLibros.getSelectedValue().toString();
                //model.modificarLibro(nuevoLibro,codigo);
                limpiarGUI();
                refrescarTabla();
                modoEdicion(true);
                break;

            case "Eliminar":
                // 1. Preguntar al usuario
                if (JOptionPane.showConfirmDialog(null,
                        "¿Está seguro?", "Eliminar", JOptionPane.YES_NO_OPTION)
                        == JOptionPane.NO_OPTION)
                    return;

                // 2. Eliminar
                String elimi= String.valueOf(viewLibro.tableLibros.getValueAt(fila, 0));

                model.eliminarLibro(elimi);
                libros.remove(model.buscarEliminarLibro(elimi));
                // 3. Refrescar
                refrescarTabla();
                limpiarGUI();
                break;

            case "Terminar":
                viewLibro.frame.dispose();
                break;
        }
        refrescarTabla();
        modoEdicion(true);
    }

    private void refrescarTabla() {

        Object[] fila = null;
        //clear, refrescar lista
        viewLibro.dtmLibros.setNumRows(0);
        for (Libro libro : libros){
            fila = new Object[]{libro.getCodLibro(),libro.getAutor(),libro.getAño().toString(),libro.getGeneroLi().toString(),String.valueOf(libro.getEdad())};
            viewLibro.dtmLibros.addRow(fila);
        }


    }

    /**
     * METODO PARA LIMPIAR LAS CAJAS DE TEXTO
     */
    private void limpiarGUI() {
        viewLibro.tfAutor.setText("");
        viewLibro.tfCodLibro.setText("");
        viewLibro.tfEdad.setText("");
        viewLibro.dtLibro.setDate(null);
        viewLibro.cbGeneroLibros.setSelectedItem(null);
    }

    private void modoEdicion(boolean edicion) {

        viewLibro.btAlta.setEnabled(edicion);
        viewLibro.btEliminar.setEnabled(!edicion);
        viewLibro.btModificar.setEnabled(!edicion);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

        int fila = viewLibro.tableLibros.getSelectedRow();

        if(fila!=-1) {
            viewLibro.tfCodLibro.setText((String) viewLibro.tableLibros.getValueAt(fila, 0));
            viewLibro.tfAutor.setText((String) viewLibro.tableLibros.getValueAt(fila, 1));
            viewLibro.dtLibro.setDate(new Date());
            viewLibro.cbGeneroLibros.setSelectedItem(viewLibro.tableLibros.getValueAt(fila, 3));
            viewLibro.tfEdad.setText(String.valueOf(viewLibro.tableLibros.getValueAt(fila, 4)));
        }
        modoEdicion(false);

    }
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    /**
     * METODO PARA BUSCAR EN LA LISTA
     */
    @Override
    public void keyReleased(KeyEvent e) {
        String cadenaBusqueda=viewLibro.tfBusquedaLibro.getText();

        if(cadenaBusqueda.length()>3){
            model.buscarLibro(cadenaBusqueda);
            viewLibro.tfBusquedaLibro.setText("");
        }

    }
}
