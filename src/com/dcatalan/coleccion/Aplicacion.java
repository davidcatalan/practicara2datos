package com.dcatalan.coleccion;

import java.io.IOException;

/**
 * Created by davidcatalanlisa on 3/11/16.
 */
public class Aplicacion {

    public static void main (String args[]) throws IOException, ClassNotFoundException {
        Ventana view = new Ventana();
        VentanaModel model= new VentanaModel();
        VentanaController controller= new VentanaController(view,model);
    }
}
