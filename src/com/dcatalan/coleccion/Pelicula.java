package com.dcatalan.coleccion;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by davidcatalanlisa on 3/11/16.
 */
public class Pelicula implements Serializable{

    public enum GeneroPe {
        ACCION,AVENTURAS,COMEDIA,TERROR,ROMANNCE
    }

    public enum TipoPe {
        BLUERAY,DVD,VHS
    }
    String codPelicula;
    String director;
    Date año;
    GeneroPe generoPe;
    TipoPe tipoPe;

    public Pelicula() {

    }

    public String getCodPelicula() {
        return codPelicula;
    }

    public void setCodPelicula(String codPelicula) {
        this.codPelicula = codPelicula;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public Date getAño() {
        return año;
    }

    public void setAño(Date año) {
        this.año = año;
    }

    public GeneroPe getGeneroPe() {
        return generoPe;
    }

    public void setGeneroPe(GeneroPe generoPe) {
        this.generoPe = generoPe;
    }

    public TipoPe getTipoPe() {
        return tipoPe;
    }

    public void setTipoPe(TipoPe tipoPe) {
        this.tipoPe = tipoPe;
    }

    @Override
    public String toString() {
        return codPelicula;
    }
}
