package com.dcatalan.coleccion;

import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;


/**
 * Created by davidcatalanlisa on 3/11/16.
 */
public class VentanaLibro {
    JTextField tfCodLibro;
    JTextField tfAutor;
    JTextField tfEdad;
    JComboBox cbGeneroLibros;
    JButton btAlta;
    JButton btModificar;
    JButton btEliminar;
    JButton btTerminar;
    JPanel panelLibro;
    JDateChooser dtLibro;
    JTextField tfBusquedaLibro;
    JScrollPane sbLibros;
    JTable tableLibros;
    JFrame frame;
    DefaultTableModel dtmLibros;

    public VentanaLibro() {
        frame = new JFrame("Ventana Libros");
        frame.setContentPane(panelLibro);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);



        dtmLibros = new DefaultTableModel(){
        @Override
        public boolean isCellEditable (int row, int column){
            return false;
            }
        };

        dtmLibros.addColumn("Codigo Libro");
        dtmLibros.addColumn("Autor");
        dtmLibros.addColumn("Año");
        dtmLibros.addColumn("Genero Libro");
        dtmLibros.addColumn("Edad");
        tableLibros.setModel(dtmLibros);
    }

}
