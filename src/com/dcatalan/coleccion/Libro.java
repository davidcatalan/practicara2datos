package com.dcatalan.coleccion;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by davidcatalanlisa on 3/11/16.
 */
public class Libro implements Serializable{

    public enum GeneroLi {
        ROMANCE,COMEDIA,COMIC,INFANTIL,FANTASIA
    }
    private String codLibro;
    private String  autor;
    private Date año;
    GeneroLi generoLi;
    private int edad;

    public Libro(){
    }

    public String getCodLibro() {return codLibro; }

    public void setCodLibro(String codLibro) {
        this.codLibro = codLibro;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public Date getAño() {
        return año;
    }

    public void setAño(Date año) {
        this.año = año;
    }

    public GeneroLi getGeneroLi() {
        return generoLi;
    }

    public void setGeneroLi(GeneroLi generoLi) {
        this.generoLi = generoLi;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }


    @Override
    public String toString() {
        return codLibro;
    }
}
