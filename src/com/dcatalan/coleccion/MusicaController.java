package com.dcatalan.coleccion;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static com.dcatalan.coleccion.VentanaModel.libros;
import static com.dcatalan.coleccion.VentanaModel.musicas;
import static com.dcatalan.coleccion.VentanaModel.peliculas;

/**
 * Created by davidcatalanlisa on 3/11/16.
 */
public class MusicaController  implements ActionListener,ListSelectionListener, KeyListener {

    private VentanaMusica viewMusica;
    private VentanaModel model;

    public MusicaController(VentanaMusica vMusica, VentanaModel model) {

        this.viewMusica=vMusica;
        this.model=model;

        addActionListener();
        addSelectionListeners();
        addKeyListener();

        rellenarComboBoxGe();
        rellenarComboBoxTi();

        inicializarTabla();

        refrescarTabla();

    }

    private void inicializarTabla() {
        viewMusica.tableMusica.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ListSelectionModel lsm = viewMusica.tableMusica.getSelectionModel();
        lsm.addListSelectionListener(this);
    }

    /**
     * RELLENAR COMBO BOX
     */
    private void rellenarComboBoxTi() {
        viewMusica.cbTipoCD.addItem("<Seleccciona el tipo>");
        for(Musica.TipoCD tipoCD : Musica.TipoCD.values()){
            viewMusica.cbTipoCD.addItem(tipoCD.name());
        }
    }

    private void rellenarComboBoxGe() {
        viewMusica.cbGeneroCD.addItem("<Seleccciona el genero>");
        for(Musica.GeneroCD generoCD : Musica.GeneroCD.values()){
            viewMusica.cbGeneroCD.addItem(generoCD.name());
        }
    }

    /**
     * AÑADIR LISTENERS
     */
    private void addKeyListener() {viewMusica.tfBusquedaMusica.addKeyListener(this);}
    private void addSelectionListeners() {
        //viewMusica.listMusica.addListSelectionListener(this);
    }

    private void addActionListener() {
        viewMusica.btAlta.addActionListener(this);
        viewMusica.btEliminar.addActionListener(this);
        viewMusica.btModificar.addActionListener(this);
        viewMusica.btTerminar.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String comando = e.getActionCommand();
        Musica nuevaMusica = null;
        Date fecha = null;
        int fila = viewMusica.tableMusica.getSelectedRow();

        switch (comando) {
            case "Alta":
                modoEdicion(true);

                if (viewMusica.tfCodMusica.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Codigo de musica OBLIGATORIO",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                if (viewMusica.cbGeneroCD.getSelectedIndex() == 0) {
                    JOptionPane.showMessageDialog(null, "Selecciona Genero",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if (viewMusica.cbTipoCD.getSelectedIndex() == 0) {
                    JOptionPane.showMessageDialog(null, "Selecciona Tipo de CD",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                if( viewMusica.dtMusica.getDate() == null) {
                    viewMusica.dtMusica.setDate( new Date());
                }

                if (viewMusica.tfArtista.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Artista OBLIGATORIO",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                fecha = viewMusica.dtMusica.getDate();
                nuevaMusica = new Musica();

                nuevaMusica.setCodMusica(viewMusica.tfCodMusica.getText());
                nuevaMusica.setArtista(viewMusica.tfArtista.getText());
                nuevaMusica.setAño(fecha);
                nuevaMusica.setTipoCD(Musica.TipoCD.valueOf((String) viewMusica.cbTipoCD.getSelectedItem()));
                nuevaMusica.setGeneroCD(Musica.GeneroCD.valueOf((String) viewMusica.cbGeneroCD.getSelectedItem()));

                model.registrarMusica(nuevaMusica);
                limpiarGUI();
                refrescarTabla();

                break;

            case "Modificar":
                modoEdicion(true);
                if (viewMusica.tfCodMusica.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Codigo de musica OBLIGATORIO",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                if (viewMusica.cbGeneroCD.getSelectedIndex() == 0) {
                    JOptionPane.showMessageDialog(null, "Selecciona Genero",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if (viewMusica.cbTipoCD.getSelectedIndex() == 0) {
                    JOptionPane.showMessageDialog(null, "Selecciona Tipo de CD",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if( viewMusica.dtMusica.getDate() == null) {
                    viewMusica.dtMusica.setDate( new Date());
                }
                //COGER EL CODIGO DE MUSICA PARA MODIFICAR ESE OBJETO
                //String codigo = viewMusica.tableMusica.getSelectedValue().toString();
                //model.modificarMusica(nuevaMusica, codigo);
                limpiarGUI();
                refrescarTabla();
                modoEdicion(true);
                break;

            case "Eliminar":
                // 1. Preguntar al usuario
                if (JOptionPane.showConfirmDialog(null,
                        "¿Está seguro?", "Eliminar", JOptionPane.YES_NO_OPTION)
                        == JOptionPane.NO_OPTION)
                    return;

                // 2. Eliminar
                String elimi= String.valueOf(viewMusica.tableMusica.getValueAt(fila, 0));

                model.eliminarMusica(elimi);
                musicas.remove(model.buscarEliminarMusica(elimi));

                // 3. Refrescar
                refrescarTabla();
                limpiarGUI();
                modoEdicion(false);
                break;

            case "Terminar":
                viewMusica.frame.dispose();
                break;
        }
        refrescarTabla();
        modoEdicion(true);
    }

    private void refrescarTabla() {

        Object[] fila = null;
        //clear, refrescar lista
        viewMusica.dtmMusica.setNumRows(0);
        for (Musica musica: musicas){
            fila = new Object[]{musica.getCodMusica(),musica.getArtista(),musica.getAño().toString(),musica.getTipoCD().toString(),musica.getGeneroCD().toString()};
            viewMusica.dtmMusica.addRow(fila);
        }
    }

    /**
     * LIMPIAR CAJAS DE TEXTO
     */
    private void limpiarGUI() {
        viewMusica.tfArtista.setText("");
        viewMusica.tfCodMusica.setText("");
        viewMusica.cbTipoCD.setSelectedItem(null);
        viewMusica.dtMusica.setDate(null);
        viewMusica.cbGeneroCD.setSelectedItem(null);
    }

    private void modoEdicion(boolean edicion) {

        viewMusica.btAlta.setEnabled(edicion);
        viewMusica.btEliminar.setEnabled(!edicion);
        viewMusica.btModificar.setEnabled(!edicion);
    }

    public void valueChanged(ListSelectionEvent e) {

        int fila = viewMusica.tableMusica.getSelectedRow();

        if(fila!=-1) {
            viewMusica.tfCodMusica.setText((String) viewMusica.tableMusica.getValueAt(fila, 0));
            viewMusica.tfArtista.setText((String) viewMusica.tableMusica.getValueAt(fila, 1));
            viewMusica.dtMusica.setDate(new Date());
            viewMusica.cbTipoCD.setSelectedItem(viewMusica.tableMusica.getValueAt(fila,3));
            viewMusica.cbGeneroCD.setSelectedItem(viewMusica.tableMusica.getValueAt(fila, 4));
        }
        modoEdicion(false);

    }
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }


    /**
     * BUSCAR EN LA LISTA DE MUSICAS
     */
    @Override
    public void keyReleased(KeyEvent e) {
    }
}
