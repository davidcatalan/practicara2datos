package com.dcatalan.coleccion;

import javax.swing.*;
import java.awt.*;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by davidcatalanlisa on 3/11/16.
 */
public class VentanaModel extends Component {

    public static ArrayList<Libro> libros;
    public static ArrayList<Pelicula> peliculas;
    public static ArrayList<Musica> musicas;
    public static Connection conexion;

    public VentanaModel() throws IOException, ClassNotFoundException {
        conexion=conectarBBDD();
        peliculas=obtenerPeliculas();
        libros=obtenerLibros();
        musicas=obtenerMusicas();
    }

    /**
     * METODOS DE LIBROS
     */

    public void modificarLibro(Libro nuevoLibro, String codigo) {
    }

    public void registrarLibro(Libro libro) {
        libros.add(libro);
        String sentenciaSql = "INSERT INTO Libro (codLibro, autor, año, generoLi, edad) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, libro.getCodLibro());
            sentencia.setString(2, libro.getAutor());
            sentencia.setDate(3, new java.sql.Date(libro.getAño().getTime()));
            sentencia.setString(4, libro.getGeneroLi().toString());
            sentencia.setInt(5, libro.getEdad());
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }


    public ArrayList obtenerLibros() {
        String sentenciaSql = "SELECT codLibro, autor, año, generoLi, edad FROM Libro";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        libros= new ArrayList<Libro>();
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            resultado = sentencia.executeQuery();
            while (resultado.next()) {
                Libro libro = new Libro();
                libro.setCodLibro(resultado.getString(1));
                libro.setAutor(resultado.getString(2));
                libro.setAño(resultado.getDate(3));
                libro.setGeneroLi(Libro.GeneroLi.valueOf((String) resultado.getString(4)));
                libro.setEdad(resultado.getInt(5));
                libros.add(libro);
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                    resultado.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
        return libros;
    }

    public ArrayList<Libro> obtenLibros() {
        String sentenciaSql = "SELECT * FROM Libro";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;


        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            resultado = sentencia.executeQuery();
            while (resultado.next()) {
                resultado.getString(1);
                resultado.getString(2);
                resultado.getDate(3);
                resultado.getString(4);
                resultado.getInt(5);
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                    resultado.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
        return null;
    }

    public void eliminarLibro(String codLibro) {
        String sentenciaSql = "DELETE FROM Libro WHERE codLibro = '"+codLibro+"'";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }


    }

    /**
     * METODOS DE PELICULAS
     */

    public void modificarPelicula(Pelicula nuevaPelicula, String codigo) throws SQLException {

        String sentenciaSql = "UPDATE Pelicula SET codPelicula = ?, director= ?, año= ?, generoPe= ?, tipoPe= ? WHERE codPelicula= ?";
        PreparedStatement sentencia= conexion.prepareStatement(sentenciaSql);

        sentencia.setString(1, nuevaPelicula.getCodPelicula());
        sentencia.setString(2, nuevaPelicula.getDirector());
        sentencia.setDate(3, new java.sql.Date(nuevaPelicula.getAño().getTime()));
        sentencia.setString(4, nuevaPelicula.getGeneroPe().toString());
        sentencia.setString(5, nuevaPelicula.getTipoPe().toString());
        sentencia.setString(6, peliculas.get(Integer.parseInt(codigo)).getCodPelicula());

        sentencia.executeUpdate();

        if (sentencia != null)
            sentencia.close();


        /*
        Pelicula pelicula = peliculas.get(codigo);
        pelicula.setCodPelicula(nuevaPelicula.getCodPelicula());
        pelicula.setDirector(nuevaPelicula.getDirector());
        pelicula.setAño(nuevaPelicula.getAño());
        pelicula.setTipoPe(nuevaPelicula.getTipoPe());
        pelicula.setGeneroPe(nuevaPelicula.getGeneroPe());
        */
    }


    public void registrarPelicula(Pelicula pelicula) {

        peliculas.add(pelicula);
        String sentenciaSql = "INSERT INTO Pelicula (codPelicula, director, año, generoPe, tipoPe) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, pelicula.getCodPelicula());
            sentencia.setString(2, pelicula.getDirector());
            sentencia.setDate(3, new java.sql.Date(pelicula.getAño().getTime()));
            sentencia.setString(4, pelicula.getGeneroPe().toString());
            sentencia.setString(5, pelicula.getTipoPe().toString());
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    public ArrayList obtenerPeliculas() {
        String sentenciaSql = "SELECT codPelicula, director, año, generoPe, tipoPe FROM Pelicula";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        peliculas=new ArrayList<Pelicula>();

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            resultado = sentencia.executeQuery();
            while (resultado.next()) {
                Pelicula pelicula= new Pelicula();
                pelicula.setCodPelicula(resultado.getString(1));
                pelicula.setDirector(resultado.getString(2));
                pelicula.setAño(resultado.getDate(3));
                pelicula.setGeneroPe(Pelicula.GeneroPe.valueOf((String) resultado.getString(4)));
                pelicula.setTipoPe(Pelicula.TipoPe.valueOf((String) resultado.getString(5)));
                peliculas.add(pelicula);
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                    resultado.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }

        return peliculas;
    }

    public ArrayList<Pelicula> obtenPelicula() {

        String sentenciaSql = "SELECT codPelicula FROM Pelicula";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        ArrayList<Pelicula> Codigos = null;
        String nombre = null;


        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            resultado = sentencia.executeQuery();
            while (resultado.next()) {
                nombre = resultado.getString(1);
                //Codigos.add(nombre);
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                    resultado.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
        return null;
    }

    public void eliminarPelicula(String codPelicula) {

        String sentenciaSql = "DELETE FROM Pelicula WHERE codPelicula = '"+codPelicula+"'";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * METODOS DE MUSICA
     */

    public void modificarMusica(Musica nuevaMusica, String codigo) {
        //UPDATE
        /*
        Musica musica = musicas.get(codigo);
        musica.setCodMusica(nuevaMusica.getCodMusica());
        musica.setArtista(nuevaMusica.getArtista());
        musica.setAño(nuevaMusica.getAño());
        musica.setTipoCD(nuevaMusica.getTipoCD());
        musica.setGeneroCD(nuevaMusica.getGeneroCD());
        */
    }


    public void registrarMusica(Musica musica) {
        musicas.add(musica);
        String sentenciaSql = "INSERT INTO Musica (codMusica, artista, año, tipoCD, generoCD) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, musica.getCodMusica());
            sentencia.setString(2, musica.getArtista());
            sentencia.setDate(3, new java.sql.Date(musica.getAño().getTime()));
            sentencia.setString(4, musica.getTipoCD().toString());
            sentencia.setString(5, musica.getGeneroCD().toString());
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    public ArrayList obtenerMusicas() {

        String sentenciaSql = "SELECT codMusica, artista, año, tipoCD, generoCD FROM Musica";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        musicas=new ArrayList<Musica>();

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            resultado = sentencia.executeQuery();
            while (resultado.next()) {
                Musica musica= new Musica();
                musica.setCodMusica(resultado.getString(1));
                musica.setArtista(resultado.getString(2));
                musica.setAño(resultado.getDate(3));
                musica.setTipoCD(Musica.TipoCD.valueOf((String) resultado.getString(4)));
                musica.setGeneroCD(Musica.GeneroCD.valueOf((String) resultado.getString(5)));
                musicas.add(musica);
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                    resultado.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
        return musicas;

    }

    public void eliminarMusica(String codMusica) {

        String sentenciaSql = "DELETE FROM Musica WHERE codMusica = '"+codMusica+"'";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }

    }

    /**
     *          METODOS DE CONEXION Y DESCONEXION BBDD
     */

    public Connection conectarBBDD() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();

            return  conexion = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Practica2tri1",
                    "root", "");
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } catch (InstantiationException ie) {
            ie.printStackTrace();
        } catch (IllegalAccessException iae) {
            iae.printStackTrace();
        }
        return null;
    }

    public void desconectarBBDD() {
        try {
            conexion.close();
            conexion = null;
            JOptionPane.showMessageDialog(null, "Se ha desconectado de la Base de Datos");
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    public Libro buscarEliminarLibro(String elimi) {
        Libro encontrada = new Libro();
        for (Libro libro : libros) {
            if ((libro.getCodLibro().contains(elimi))) {
                encontrada = libro;
            }
        }

        return encontrada;
    }
    public Pelicula buscarEliminarPelicula(String elimi){
        Pelicula encontrada = new Pelicula();
        for(Pelicula pelicula : peliculas){
            if(pelicula.getCodPelicula().contains(elimi)){
                encontrada=pelicula;
            }
        }
        return encontrada;
    }

    public Musica buscarEliminarMusica(String elimi){
        Musica encontrada = new Musica();
        for(Musica musica : musicas){
            if(musica.getCodMusica().contains(elimi)){
                encontrada=musica;
            }
        }
        return encontrada;
    }

    public void buscarLibro(String text) {
        String consulta = "SELECT codLibro, autor FROM Libro WHERE codLibro = '"+text+"'";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        String res="";
        try {
            sentencia = conexion.prepareStatement(consulta);
            resultado = sentencia.executeQuery();

            while(resultado.next() && resultado!=null){
                res = (resultado.getString(1));
                res = res+(resultado.getString(2))+"/n";

            }
            if(res.equals("")){
                JOptionPane.showMessageDialog(null,"No hay resultados");
            }else{
                JOptionPane.showMessageDialog(null,"Se ha encontrado : "+res);
            }

            sentencia.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*
    Properties configuracion = new Properties();
		if (rbMysql.isSelected()) {
			configuracion.setProperty("driver", "com.mysql.jdbc.Driver");
			configuracion.setProperty("protocolo", "jdbc:mysql://");
		}
		else {
			configuracion.setProperty("driver", "org.postgresql.Driver");
			configuracion.setProperty("protocolo", "jdbc:postgresql://");
		}
		configuracion.setProperty("servidor", tfServidor.getText());
		configuracion.setProperty("basedatos", tfBaseDatos.getText());
		configuracion.setProperty("puerto", tfPuerto.getText());
		configuracion.setProperty("usuario", tfUsuario.getText());
		configuracion.setProperty("contrasena", tfContrasena.getText());

		try {
			configuracion.store(new FileOutputStream("configuracion.props"), "-- Ejemplo de fichero de propiedades --");
			setVisible(false);
		} catch (FileNotFoundException fnfe) {
			// TODO Mostrar mensaje de error
		} catch (IOException ioe) {
			// TODO Mostrar mensaje de error
		}
     */
}


