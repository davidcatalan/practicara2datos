package com.dcatalan.coleccion;

import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Created by davidcatalanlisa on 3/11/16.
 */
public class VentanaMusica {
    JTextField tfCodMusica;
    JTextField tfArtista;
    JComboBox cbTipoCD;
    JComboBox cbGeneroCD;
    JButton btAlta;
    JButton btModificar;
    JButton btEliminar;
    JButton btTerminar;
    JPanel panelMusica;
    JDateChooser dtMusica;
    JTextField tfBusquedaMusica;
    JScrollPane sbMusicas;
    JTable tableMusica;
    JFrame frame;
    DefaultTableModel dtmMusica;

    public VentanaMusica() {
        frame = new JFrame("Ventana Musica");
        frame.setContentPane(panelMusica);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        dtmMusica = new DefaultTableModel(){
            @Override
            public boolean isCellEditable (int row, int column){
                return false;
            }
        };

        dtmMusica.addColumn("Codigo Musica");
        dtmMusica.addColumn("Artista");
        dtmMusica.addColumn("Año");
        dtmMusica.addColumn("Tipo de CD");
        dtmMusica.addColumn("Genero de CD");

        tableMusica.setModel(dtmMusica);

    }
}
