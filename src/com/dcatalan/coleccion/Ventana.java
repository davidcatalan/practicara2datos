package com.dcatalan.coleccion;

import javax.swing.*;

/**
 * Created by davidcatalanlisa on 3/11/16.
 */
public class Ventana {
    JButton btLibro;
    JButton btPelicula;
    JButton btMusica;
    JPanel panel1;

    public Ventana() {
        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        Login dialog = new Login();
        dialog.pack();
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
    }

}
