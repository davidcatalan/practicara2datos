package com.dcatalan.coleccion;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelListener;
import java.awt.event.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import static com.dcatalan.coleccion.VentanaModel.peliculas;

/**
 * Created by davidcatalanlisa on 3/11/16.
 */
public class PeliculaController implements ActionListener,ListSelectionListener, KeyListener {

    private VentanaPeliculas viewPeliculas;
    private VentanaModel model;

    public PeliculaController(VentanaPeliculas vPeliculas, VentanaModel model) {

        this.viewPeliculas = vPeliculas;
        this.model = model;

        addActionListener();
        addKeyListener();

        rellenarComboBoxGe();
        rellenarComboBoxTi();

        inicializarTablaPe();

        refrescarTabla();
    }

    private void inicializarTablaPe() {
        viewPeliculas.tablePeliculas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ListSelectionModel lsm = viewPeliculas.tablePeliculas.getSelectionModel();
        lsm.addListSelectionListener(this);
    }

    /**
     * RELLENANDO COMBO BOX
     */
    private void rellenarComboBoxTi() {
        viewPeliculas.cbTipoPe.addItem("<Seleccciona el tipo>");
        for (Pelicula.TipoPe tipoPe : Pelicula.TipoPe.values()) {
            viewPeliculas.cbTipoPe.addItem(tipoPe.name());
        }
    }

    private void rellenarComboBoxGe() {
        viewPeliculas.cbGeneroPeliculas.addItem("<Seleccciona el genero>");
        for (Pelicula.GeneroPe generoPe : Pelicula.GeneroPe.values()) {
            viewPeliculas.cbGeneroPeliculas.addItem(generoPe.name());
        }
    }

    /**
     * AÑADIR LISTENERS
     */

    private void addKeyListener() {
        viewPeliculas.tfBusquedaPelicula.addKeyListener(this);
    }


    private void addActionListener() {
        viewPeliculas.btAlta.addActionListener(this);
        viewPeliculas.btEliminar.addActionListener(this);
        viewPeliculas.btModificar.addActionListener(this);
        viewPeliculas.btTerminar.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String comando = e.getActionCommand();
        Pelicula nuevaPelicula = null;
        Date fecha = null;
        int fila = viewPeliculas.tablePeliculas.getSelectedRow();

        switch (comando) {
            case "Alta":
                modoEdicion(true);

                if (viewPeliculas.tfCodPelicula.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Codigo de Pelicula OBLIGATORIO",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                if (viewPeliculas.cbGeneroPeliculas.getSelectedIndex() == 0) {
                    JOptionPane.showMessageDialog(null, "Selecciona Genero",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if (viewPeliculas.cbTipoPe.getSelectedIndex() == 0) {
                    JOptionPane.showMessageDialog(null, "Selecciona Tipo de Pelicula",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if (viewPeliculas.dtPeliculas.getDate() == null) {
                    viewPeliculas.dtPeliculas.setDate(new Date());
                }
                if (viewPeliculas.tfDirector.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Director OBLIGATORIO",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                fecha = viewPeliculas.dtPeliculas.getDate();
                nuevaPelicula = new Pelicula();

                nuevaPelicula.setCodPelicula(viewPeliculas.tfCodPelicula.getText());
                nuevaPelicula.setDirector(viewPeliculas.tfDirector.getText());
                nuevaPelicula.setAño(fecha);
                nuevaPelicula.setTipoPe(Pelicula.TipoPe.valueOf((String) viewPeliculas.cbTipoPe.getSelectedItem()));
                nuevaPelicula.setGeneroPe(Pelicula.GeneroPe.valueOf((String) viewPeliculas.cbGeneroPeliculas.getSelectedItem()));

                model.registrarPelicula(nuevaPelicula);


                limpiarGUI();
                refrescarTabla();

                break;

            case "Modificar":
                modoEdicion(true);
                if (viewPeliculas.tfCodPelicula.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Codigo de PElicula OBLIGATORIO",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                if (viewPeliculas.cbGeneroPeliculas.getSelectedIndex() == 0) {
                    JOptionPane.showMessageDialog(null, "Selecciona Genero",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if (viewPeliculas.cbTipoPe.getSelectedIndex() == 0) {
                    JOptionPane.showMessageDialog(null, "Selecciona Tipo de Pelicula",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if (viewPeliculas.dtPeliculas.getDate() == null) {
                    viewPeliculas.dtPeliculas.setDate(new Date());
                }
                //COGER EL CODIGO DE PELICULA PARA
                String codigo = (String) viewPeliculas.tablePeliculas.getValueAt(viewPeliculas.tablePeliculas.getSelectedRow(), 0);
                try {
                    model.modificarPelicula(nuevaPelicula, codigo);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

                limpiarGUI();
                refrescarTabla();
                modoEdicion(true);
                break;

            case "Eliminar":
                // 1. Preguntar al usuario
                if (JOptionPane.showConfirmDialog(null,
                        "¿Está seguro?", "Eliminar", JOptionPane.YES_NO_OPTION)
                        == JOptionPane.NO_OPTION)
                    return;

                // 2. Eliminar
                String elimi=String.valueOf(viewPeliculas.tablePeliculas.getValueAt(fila,0));

                model.eliminarPelicula(elimi);
                peliculas.remove(model.buscarEliminarPelicula(elimi));
                // 3. Refrescar
                refrescarTabla();
                limpiarGUI();
                modoEdicion(false);
                break;

            case "Terminar":
                viewPeliculas.frame.dispose();
                break;
        }
        refrescarTabla();
        modoEdicion(true);
    }

    private void refrescarTabla() {
        Object[] fila = null;
        //clear, refrescar lista
        viewPeliculas.dtmPeliculas.setNumRows(0);
        for (Pelicula pelicula : peliculas) {
            fila = new Object[]{pelicula.getCodPelicula(), pelicula.getDirector(), pelicula.getAño().toString(), pelicula.getGeneroPe().toString(), pelicula.getTipoPe().toString()};
            viewPeliculas.dtmPeliculas.addRow(fila);
        }

    }

    /**
     * METODO LIMPIA CAJAS DE TEXTO
     */
    private void limpiarGUI() {
        viewPeliculas.tfDirector.setText("");
        viewPeliculas.tfCodPelicula.setText("");
        viewPeliculas.cbTipoPe.setSelectedItem(null);
        viewPeliculas.dtPeliculas.setDate(null);
        viewPeliculas.cbGeneroPeliculas.setSelectedItem(null);
    }

    private void modoEdicion(boolean edicion) {

        viewPeliculas.btAlta.setEnabled(edicion);
        viewPeliculas.btEliminar.setEnabled(!edicion);
        viewPeliculas.btModificar.setEnabled(!edicion);
    }

    public void valueChanged(ListSelectionEvent e) {

        int fila = viewPeliculas.tablePeliculas.getSelectedRow();

        if(fila!=-1) {
            viewPeliculas.tfCodPelicula.setText((String) viewPeliculas.tablePeliculas.getValueAt(fila, 0));
            viewPeliculas.tfDirector.setText((String) viewPeliculas.tablePeliculas.getValueAt(fila, 1));
            viewPeliculas.dtPeliculas.setDate(new Date());
            viewPeliculas.cbGeneroPeliculas.setSelectedItem(viewPeliculas.tablePeliculas.getValueAt(fila, 3));
            viewPeliculas.cbTipoPe.setSelectedItem(viewPeliculas.tablePeliculas.getValueAt(fila,4));
        }
        modoEdicion(false);

    }
    /**
     * BUSQUEDA EN LA LISTA DE PELICULAS
     */
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}