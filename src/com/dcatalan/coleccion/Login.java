package com.dcatalan.coleccion;

import javax.swing.*;
import java.awt.event.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Login extends JDialog {
    private JPanel contentPane;
    private JTextField tfUsuario;
    private JPasswordField pfContraseña;
    private JButton btCancelar;
    private JButton btAceptar;
    private JButton buttonOK;
    private JButton buttonCancel;
    Properties config;
    String user;
    String pass;

    /**
     * CLASE LA CUAL CREA UNA VENTANA PARA INTRODUCIR USUARIO  CONTRASEÑA EN CASO DE NO INTRODICRLOS BIEN
     * NO ENNTRA EN LA APLICACION
     */

    public Login() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(btAceptar);

        config= new Properties();
        config.setProperty("user", "david");
        config.setProperty("password", "1234");
        try {
            config.store(new FileOutputStream("config.props"), "Usuarios");
            config.load(new FileInputStream("config.props"));
            user = config.getProperty("user");
            pass = config.getProperty("password");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        btAceptar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        btCancelar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        if (tfUsuario.getText().equals(user) && String.valueOf(pfContraseña.getPassword()).equals(pass)){
            dispose();
        } else {
            JOptionPane.showMessageDialog(null, "Usuario o contraseña INCORRECTOS",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void onCancel() {
        System.exit(0);
    }
}
